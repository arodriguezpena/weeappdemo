//
//  ViewController.swift
//  WeeAppQR
//
//  Created by Alejandro  Rodriguez on 8/19/19.
//  Copyright © 2019 Tech. All rights reserved.
//

import UIKit
import WeeQR
class ViewController: UIViewController {

    @IBOutlet weak var dataLabel: UILabel!
    
    lazy var weeQR: WeeQRControllerLogic = {
        var vc = WeeQRFactory().getController()
        vc.delegate = self
        return vc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func tapQR(_ sender: Any) {
        self.present((weeQR as! UIViewController), animated: true)
    }
}
extension ViewController: WeeQRDelegate {
    func getQR(_ code: String) {
          dataLabel.text = "Code: \(code)"
    }
    func getError(_ error: WeeQRError) {
        dataLabel.text = "Error: \(error.str)"
    }
}

